package com.demouser.demouser.service;

import java.util.Date;
import com.demouser.demouser.model.User;
import org.springframework.stereotype.Service;
import io.jsonwebtoken.Jwts;

@Service
public class TokenService {
    
    public String create(User User) {
        return Jwts.builder()
                .setSubject(User.getId())
                .setExpiration(new Date(System.currentTimeMillis() + 100000))
                .compact();
    }
}