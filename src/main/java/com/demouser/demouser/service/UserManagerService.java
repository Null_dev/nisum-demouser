package com.demouser.demouser.service;

import java.util.UUID;

import com.demouser.demouser.model.User;
import com.demouser.demouser.store.UserStore;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserManagerService {
    @Autowired
    private UserStore userStore;
    @Autowired
    private TokenService tokenService;

    public User addUser(User nUser) {
        
        nUser.setId(UUID.randomUUID().toString());
        nUser.setActive(true);
        userStore.save(nUser);

        nUser.setToken(tokenService.create(nUser));
        nUser.setPassword("");
        return nUser;
    }
}